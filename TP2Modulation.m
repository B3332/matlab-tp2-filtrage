close all;

%% Param�tres
borneA = -25;
borneB = 25;
nombreEchantillion = 65536;
periodeEchantillionnage = (borneB-borneA)/nombreEchantillion;
frequenceEchantillionage = 1/periodeEchantillionnage;

%% Calcul abscisse
x = -25:periodeEchantillionnage:25-periodeEchantillionnage;
Xfreq = -frequenceEchantillionage/2:frequenceEchantillionage/nombreEchantillion:frequenceEchantillionage/2-frequenceEchantillionage/nombreEchantillion;

%% Calcul fonction
%nombrePerdiodeIntervalle = 5.4; %Nombre entier 0,1,2,3, ...
%f0 = nombrePerdiodeIntervalle/(borneB-borneA);
%f0 = 20; % Pour le d�finir � la main
%beta = 1;
%thau = 0.05;

y1 = (6)*cos(2*pi*5*0*x);
for n=1:5
y1 = y1 + (6-n)*cos(2*pi*5*n*x);
end


y2 = (1)*cos(2*pi*5*1*x);
for n=2:5
y2 = y2 + (6-n)*cos(2*pi*5*n*x);
end

%% Calcul transform�es 
Y1 = tfour(y1);
Y2 = tfour(y2);

%% Cr�ation de la porteuse
f1 = 100;
f2 = 200;
porteusef1 = cos(2*pi*f1*x);
porteusef2 = cos(2*pi*f2*x);
porteuseF1 = tfour(porteusef1);
porteuseF2 = tfour(porteusef2);

%% Modulation
Y1moduled = tfour(porteusef1.*y1);
Y2moduled = tfour(porteusef2.*y2);

%% Cr�ation de c(t)
c = Y1moduled + Y2moduled;

%% D�modulation
Y1demoduladed = conv(c, porteuseF1,'same');
Y2demoduladed = conv(c, porteuseF2,'same');

%% HeaviSide pour nettoyer
thau = 5;
passebas = heaviside(x+(thau/2))-heaviside(x-(thau/2)); % Normalement : (2016b) rectangularPulse(-thau,thau,x);

%% Nettoyage
Y1nettoye = Y1demoduladed.*passebas;
Y2nettoye = Y2demoduladed.*passebas;

%% Affichage des fonctions
figure % opens new figure window
title('Affichage des signaux de bases')
nbLigne1 = 2;
nbColon1 = 2;
subplot(nbLigne1,nbColon1,1); plot(x,real(y1),x,imag(y1)); title('Signal 1'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,2); plot(x,real(y2),x,imag(y2)); title('Signal 2'); xlabel('temps'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,3); plot(Xfreq,real(Y1), Xfreq,imag(Y1)); title('Signal 1 TF'); xlabel('fr�quence'); ylabel('amplitude'); legend('r�elle','imaginaire');
subplot(nbLigne1,nbColon1,4); plot(Xfreq,real(Y2), Xfreq,imag(Y2)); title('Signal 2 TF'); xlabel('fr�quence'); ylabel('amplitude'); legend('r�elle','imaginaire');

%% Affichage des fonctions modul�es
figure % opens new figure window
title('Affichage des signaux modul�s')
nbLigne1 = 1;
nbColon1 = 2;
subplot(nbLigne1,nbColon1,1); plot(Xfreq,real(porteuseF1), Xfreq,real(porteuseF2)); title('Porteuses F1 et F2'); xlabel('fr�quence'); ylabel('amplitude'); legend('porteuse F1','porteuse F2');
subplot(nbLigne1,nbColon1,2); plot(Xfreq,real(Y1moduled),Xfreq,real(Y2moduled)); title('Signal 1 et 2 modul�s'); xlabel('fr�quence'); ylabel('amplitude'); legend('Signal 1 modul� r�el','Signal 2 modul� r�el');


%% Affichage des fonctions d�modul�es
figure % opens new figure window
title('Affichage de la d�modulation')
nbLigne1 = 2;
nbColon1 = 3;
subplot(nbLigne1,nbColon1,1); plot(Xfreq,real(c)); title('Signal transmis'); xlabel('fr�quence'); ylabel('amplitude'); legend('Signal S1 et S2 "ensemble"');
subplot(nbLigne1,nbColon1,2); plot(Xfreq,real(Y1demoduladed)); title('Signal 1 demodul�s'); xlabel('fr�quence'); ylabel('amplitude'); legend('Signal 1 d�modul� r�el');
subplot(nbLigne1,nbColon1,3); plot(Xfreq,real(Y2demoduladed)); title('Signal 2 demodul�s'); xlabel('fr�quence'); ylabel('amplitude'); legend('Signal 2 d�modul� r�el');
subplot(nbLigne1,nbColon1,4); plot(Xfreq,real(passebas)); title('Passe-bas'); xlabel('fr�quence'); ylabel('amplitude'); legend('Passe-bas');
subplot(nbLigne1,nbColon1,5); plot(Xfreq,real(Y1nettoye)); title('Signal 1 nettoy�'); xlabel('fr�quence'); ylabel('amplitude'); legend('Signal 1 nettoy� r�el');
subplot(nbLigne1,nbColon1,6); plot(Xfreq,real(Y2nettoye)); title('Signal 2 nettoy�'); xlabel('fr�quence'); ylabel('amplitude'); legend('Signal 2 nettoy� r�el');


