close all;

%% R�cup�ration de l'image et des infos
[im, map] = imread('photo-floue.png');
[imRef, mapRef] = imread('photo-ref.png');

size(im)
size(map)
mapndg=([0:255]/255)'*[1 1 1];

%% Calcul de la FFT
IM=fftshift(fft2(im)); % FFT de l'imge avec FFT shift pour que les fr�quences aillent de -fe/2 � fe/2

%% Param�tres du flou
nbPixel = 21;

%% Simulation du flou
h = HGenerator(nbPixel,512);
H = fftshift(fft2(h))/nbPixel;

%% R�cup�ration du bruit
imRefFREQ = fftshift(fft2(imRef));
imRefFREQFlou = (imRefFREQ .* H);
imRefFlou = ifft2(fftshift(imRefFREQFlou));

Bruit = imRefFlou - round(imRefFlou);
BruitFREQ = fftshift(fft2(Bruit));

%% Wiener 
SpectrePuissanceBruit = power(abs(BruitFREQ),2);
SpectrePuissanceImage = power(abs(imRefFREQ),2); % Freqflou ?
SpectrePuissanceH = power(abs(H),2);

FiltreWiener = SpectrePuissanceH./(H.*(SpectrePuissanceH+(SpectrePuissanceBruit./SpectrePuissanceImage)));

ImFinalFREQ = FiltreWiener.*IM;
imFinal = ifft2(fftshift(ImFinalFREQ));

%% Repr�sentation du spectre d'amplitude (�chelle log)
affIM=abs(IM)+1; % 1 � cause du log
maxi=max(max(affIM));
mini=min(min(affIM));
%La dynamique est �cras�e par les grands �carts de la TF (quelques valeurs
%tr�s �lev�es)
affIM=(log(affIM)-log(mini))/(log(maxi)-log(mini))*255; % Valeur entre 0 et 1 * 255

%% Affichage original
figure % opens new figure window
title('Affichage images originales')
nbLigne1 = 1;
nbColon1 = 2;
subplot(nbLigne1,nbColon1,1); image(im); title('Image Originale');
subplot(nbLigne1,nbColon1,2); image(affIM);  title('Spectre Original');
colormap(mapndg)

%% Affichage
figure % opens new figure window
title('Affichage du filtrage')
nbLigne1 = 1;
nbColon1 = 2;
subplot(nbLigne1,nbColon1,1); image(imRef); title('Image R�f�rence');
subplot(nbLigne1,nbColon1,2); image(imRefFlou);  title('Image R�f�rence Floue');
colormap(mapndg)

%% Affichage final
figure;
image(abs(imFinal));
colormap(mapndg)